# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2014, 2015, 2018, 2019, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2023-06-10 09:19+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.12.3\n"

#: contents/ui/BarcodePage.qml:23
#, kde-format
msgid "QR Code"
msgstr "Kod QR"

#: contents/ui/BarcodePage.qml:24
#, kde-format
msgid "Data Matrix"
msgstr "Macierz danych"

#: contents/ui/BarcodePage.qml:25
#, kde-format
msgctxt "Aztec barcode"
msgid "Aztec"
msgstr "Aztec"

#: contents/ui/BarcodePage.qml:26
#, kde-format
msgid "Code 39"
msgstr "Kod 39"

#: contents/ui/BarcodePage.qml:27
#, kde-format
msgid "Code 93"
msgstr "Kod 93"

#: contents/ui/BarcodePage.qml:28
#, kde-format
msgid "Code 128"
msgstr "Kod 128"

#: contents/ui/BarcodePage.qml:44
#, kde-format
msgid "Return to Clipboard"
msgstr "Wróć do schowka"

#: contents/ui/BarcodePage.qml:79
#, kde-format
msgid "Change the QR code type"
msgstr "Zmień rodzaj kodu QR"

#: contents/ui/BarcodePage.qml:136
#, kde-format
msgid "Creating QR code failed"
msgstr "Nie udało się utworzyć kodu QR"

#: contents/ui/BarcodePage.qml:147
#, kde-format
msgid ""
"There is not enough space to display the QR code. Try resizing this applet."
msgstr ""
"Zbyt mało miejsca, aby wyświetlić kod QR. Spróbuj zmienić rozmiar apletu."

#: contents/ui/DelegateToolButtons.qml:23
#, kde-format
msgid "Invoke action"
msgstr "Wywołaj działanie"

#: contents/ui/DelegateToolButtons.qml:38
#, kde-format
msgid "Show QR code"
msgstr "Pokaż kod QR"

#: contents/ui/DelegateToolButtons.qml:54
#, kde-format
msgid "Edit contents"
msgstr "Edytuj zawartość"

#: contents/ui/DelegateToolButtons.qml:68
#, kde-format
msgid "Remove from history"
msgstr "Usuń z historii"

#: contents/ui/EditPage.qml:82
#, kde-format
msgctxt "@action:button"
msgid "Save"
msgstr "Zapisz"

#: contents/ui/EditPage.qml:90
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Anuluj"

#: contents/ui/main.qml:30
#, kde-format
msgid "Clipboard Contents"
msgstr "Zawartość schowka"

#: contents/ui/main.qml:31 contents/ui/Menu.qml:134
#, kde-format
msgid "Clipboard is empty"
msgstr "Schowek jest pusty"

#: contents/ui/main.qml:59
#, kde-format
msgid "Clear History"
msgstr "Wyczyść historię"

#: contents/ui/main.qml:71
#, kde-format
msgid "Configure Clipboard…"
msgstr "Ustawienia schowka..."

#: contents/ui/Menu.qml:134
#, kde-format
msgid "No matches"
msgstr "Brak pasujących wyrażeń"

#: contents/ui/UrlItemDelegate.qml:106
#, kde-format
msgctxt ""
"Indicator that there are more urls in the clipboard than previews shown"
msgid "+%1"
msgstr "+%1"

#~ msgid "The QR code is too large to be displayed"
#~ msgstr "Kod QR jest zbyt duży, aby go wyświetlić"

#~ msgid "Clear history"
#~ msgstr "Wyczyść historię"

#~ msgid "Search…"
#~ msgstr "Szukaj…"

#~ msgid "Clipboard history is empty."
#~ msgstr "Historia schowka jest pusta."

#~ msgid "Configure"
#~ msgstr "Ustawienia"
