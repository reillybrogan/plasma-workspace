# Lithuanian translations for plasma_wallpaper_image package.
# This file is distributed under the same license as the plasma_wallpaper_image package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2008.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2012, 2014.
# Liudas Alisauskas <liudas@akmc.lt>, 2013, 2015.
# liudas@aksioma.lt <liudas@aksioma.lt>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-26 01:47+0000\n"
"PO-Revision-Date: 2022-11-18 00:42+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "<>"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, fuzzy, kde-format
#| msgid "Open Image"
msgctxt "@title:window"
msgid "Open Image"
msgstr "Atverti paveikslą"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, fuzzy, kde-format
#| msgid "Directory with the wallpaper to show slides from"
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Katalogas su darbalaukio fonais skaidrių rodymui"

#: imagepackage/contents/ui/config.qml:100
#, kde-format
msgid "Positioning:"
msgstr "Išdėstymas:"

#: imagepackage/contents/ui/config.qml:103
#, kde-format
msgid "Scaled and Cropped"
msgstr "Pakeisto mastelio ir apkarpytas"

#: imagepackage/contents/ui/config.qml:107
#, kde-format
msgid "Scaled"
msgstr "Pakeisto mastelio"

#: imagepackage/contents/ui/config.qml:111
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Pakeisto mastelio, išlaikytų proporcijų"

#: imagepackage/contents/ui/config.qml:115
#, kde-format
msgid "Centered"
msgstr "Centruotas"

#: imagepackage/contents/ui/config.qml:119
#, kde-format
msgid "Tiled"
msgstr "Išklotas"

#: imagepackage/contents/ui/config.qml:147
#, kde-format
msgid "Background:"
msgstr "Fonas:"

#: imagepackage/contents/ui/config.qml:148
#, kde-format
msgid "Blur"
msgstr "Suliejimas"

#: imagepackage/contents/ui/config.qml:157
#, kde-format
msgid "Solid color"
msgstr "Vientisa spalva"

#: imagepackage/contents/ui/config.qml:167
#, kde-format
msgid "Select Background Color"
msgstr "Pasirinkti fono spalvą"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Atverti darbalaukio fono paveikslą"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Kitas darbalaukio fono paveikslas"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, fuzzy, kde-format
#| msgid "Image Files"
msgid "Images"
msgstr "Paveikslų failai"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr ""

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, fuzzy, kde-format
#| msgid "Get New Wallpapers…"
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Gauti naujų darbalaukio fonų…"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Atverti vidinį aplanką"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Atkurti darbalaukio foną"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Šalinti darbalaukio foną"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""
"Šis įrankis leidžia jums nustatyti paveikslą kaip Plasma seanso darbalaukio "
"foną."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Paveikslo failas ar įdiegtas darbalaukio fono kpackage paketas, kurį norite "
"nustatyti kaip darbalaukio foną savo Plasma seansui"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"Šio darbalaukio fono failo pavadinime yra pašalinė kabutė (') - susisiekite "
"su darbalaukio fono autoriumi, kad jis tai pataisytų arba patys pervadinkite "
"failą: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "Bandant nustatyti Plasma darbalaukio foną, įvyko klaida:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""
"KPackage paketas, paremtas %1, sėkmingai nustatytas visų darbalaukių fonu"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "Paveikslas %1 sėkmingai nustatytas visų darbalaukių fonu"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"Failo, kuris buvo perduotas, kad būtų nustatytas darbalaukio fonu, nėra, "
"arba nepavyksta jo atpažinti kaip darbalaukio fono: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Paveikslų failai"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:37
#, kde-format
msgid "Order:"
msgstr "Tvarka:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:44
#, kde-format
msgid "Random"
msgstr "Atsitiktinė"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "A to Z"
msgstr "Nuo A iki Ž"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Z to A"
msgstr "Nuo Ž iki A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "Date modified (newest first)"
msgstr "Modifikavimo data (iš pradžių, naujausi)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Modifikavimo data (iš pradžių, seniausi)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:85
#, kde-format
msgid "Group by folders"
msgstr "Grupuoti pagal aplankus"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:97
#, kde-format
msgid "Change every:"
msgstr "Keisti kas:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:107
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 valandą"
msgstr[1] "%1 valandas"
msgstr[2] "%1 valandų"
msgstr[3] "%1 valandą"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:127
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minutę"
msgstr[1] "%1 minutes"
msgstr[2] "%1 minučių"
msgstr[3] "%1 minutę"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:147
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundę"
msgstr[1] "%1 sekundes"
msgstr[2] "%1 sekundžių"
msgstr[3] "%1 sekundę"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:178
#, kde-format
msgid "Folders"
msgstr "Aplankai"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:182
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, kde-format
msgid "Remove Folder"
msgstr "Šalinti aplanką"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, fuzzy, kde-format
#| msgid "Open Folder"
msgid "Open Folder…"
msgstr "Atverti aplanką"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:246
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "Nėra sukonfigūruotų darbalaukio fono vietų"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, fuzzy, kde-format
#| msgid "Get New Wallpapers…"
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Gauti naujų darbalaukio fonų…"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, fuzzy, kde-kuit-format
#| msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr "Bandant nustatyti Plasma darbalaukio foną, įvyko klaida:\n"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, fuzzy, kde-format
#| msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr "Bandant nustatyti Plasma darbalaukio foną, įvyko klaida:\n"

#~ msgid "Add Image…"
#~ msgstr "Pridėti paveikslą…"

#~ msgid "Add Folder…"
#~ msgstr "Pridėti aplanką…"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Rekomenduojamas darbalaukio fono failas"

#~ msgid "There are no wallpapers in this slideshow"
#~ msgstr "Šiame skaidrių rodyme nėra jokių paveikslų"

#~ msgid "Add Custom Wallpaper"
#~ msgstr "Pridėti tinkintą darbalaukio foną"

#~ msgid "Remove wallpaper"
#~ msgstr "Šalinti darbalaukio foną"

#~ msgid "%1 by %2"
#~ msgstr "%1, sukūrė: %2"

#~ msgid "Wallpapers"
#~ msgstr "Darbalaukio fonai"

#~ msgctxt "<image> by <author>"
#~ msgid "By %1"
#~ msgstr "Pagal %1"

#~ msgid "Download Wallpapers"
#~ msgstr "Atsisiųsti darbalaukio fonų"

#~ msgid "Hours"
#~ msgstr "val."

#~ msgid "Open..."
#~ msgstr "Atverti..."

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Nežinoma"
