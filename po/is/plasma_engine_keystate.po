# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-10 00:46+0000\n"
"PO-Revision-Date: 2022-10-17 19:03+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: keystate.cpp:15
msgid "Shift"
msgstr "Shift"

#: keystate.cpp:16
msgid "Ctrl"
msgstr "Ctrl"

#: keystate.cpp:17
msgid "Alt"
msgstr "Alt"

#: keystate.cpp:18
msgid "Meta"
msgstr "Meta"

#: keystate.cpp:19
msgid "Super"
msgstr "Super"

#: keystate.cpp:20
msgid "Hyper"
msgstr "Hyper"

#: keystate.cpp:21
msgid "AltGr"
msgstr "AltGr"

#: keystate.cpp:22
msgid "Num Lock"
msgstr "Num Lock"

#: keystate.cpp:23
msgid "Caps Lock"
msgstr "Caps Lock"

#: keystate.cpp:24
msgid "Scroll Lock"
msgstr "Scroll Lock"

#: keystate.cpp:26
msgid "Left Button"
msgstr "Vinstri hnappur"

#: keystate.cpp:27
msgid "Right Button"
msgstr "Hægri hnappur"

#: keystate.cpp:28
msgid "Middle Button"
msgstr "Miðhnappur"

#: keystate.cpp:29
msgid "First X Button"
msgstr "Fyrsti X-hnappur"

#: keystate.cpp:30
msgid "Second X Button"
msgstr "Annar X-hnappur"

#: keystate.cpp:44 keystate.cpp:54 keystate.cpp:81 keystate.cpp:102
#: keystate.cpp:110
msgid "Pressed"
msgstr "Ýtt"

#: keystate.cpp:45 keystate.cpp:88 keystate.cpp:111
msgid "Latched"
msgstr "Festur"

#: keystate.cpp:46 keystate.cpp:95 keystate.cpp:112
msgid "Locked"
msgstr "Læstur"
