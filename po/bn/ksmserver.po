# translation of ksmserver.po to Bengali
# Deepayan Sarkar <deepayan@bengalinux.org>, 2004.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2004-05-23 15:51-0500\n"
"Last-Translator: Deepayan Sarkar <deepayan@bengalinux.org>\n"
"Language-Team: Bengali <kde-translation@bengalinux.org>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: logout.cpp:273
#, kde-format
msgid "Logout canceled by '%1'"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:71 main.cpp:80
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:74
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:82
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:87
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:90
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:104 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:106 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:116 main.cpp:130
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:119 main.cpp:133
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:150
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:153
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:160
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:194
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"নির্ভরযোগ্য কে.ডি.ই. সেশন ম্যানেজার যা প্রমিত (Standard) X11R6 \n"
"সেশন ম্যানেজমেন্ট প্রোটোকল(XSMP)-র সঙ্গে যোগাযোগ করে।"

#: main.cpp:198
#, kde-format
msgid "Restores the saved user session if available"
msgstr "ব্যবহারকারীর সংরক্ষিত সেশন পাওয়া গেলে তা পুনঃস্থাপিত করে"

#: main.cpp:201
#, fuzzy, kde-format
msgid "Also allow remote connections"
msgstr "রিমোট কানেকশনও অনুমোদন করো।"

#: main.cpp:204
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:208
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:813
#, kde-format
msgctxt "@label an unknown executable is using resources"
msgid "[unknown]"
msgstr ""

#: server.cpp:836
#, kde-kuit-format
msgctxt "@label notification; %1 is a list of executables"
msgid ""
"Unable to manage some apps because the system's session management resources "
"are exhausted. Here are the top three consumers of session resources:\n"
"%1"
msgstr ""

#: server.cpp:1108
#, kde-kuit-format
msgctxt "@label notification; %1 is an executable name"
msgid ""
"Unable to restore <application>%1</application> because it is broken and has "
"exhausted the system's session restoration resources. Please report this to "
"the app's developers."
msgstr ""

#, fuzzy
#~| msgid "The KDE Session Manager"
#~ msgid "Session Management"
#~ msgstr "কে.ডি.ই. সেশন ম্যানেজার"

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "অন্য কোন উইণ্ডো ম্যানেজার সেশন-এ অংশগ্রহণ না \n"
#~ "করলে 'wm' চালু করে। ডিফল্ট 'kwin'"

#, fuzzy
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "কমপি&উটার বন্ধ করো"
#~ msgstr[1] "কমপি&উটার বন্ধ করো"

#, fuzzy
#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "কমপি&উটার বন্ধ করো"
#~ msgstr[1] "কমপি&উটার বন্ধ করো"

#, fuzzy
#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "কমপি&উটার বন্ধ করো"
#~ msgstr[1] "কমপি&উটার বন্ধ করো"

#, fuzzy
#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "কমপি&উটার বন্ধ করো"
#~ msgstr[1] "কমপি&উটার বন্ধ করো"

#, fuzzy
#~ msgid "Turn Off Computer"
#~ msgstr "কমপি&উটার বন্ধ করো"

#, fuzzy
#~ msgid "Restart Computer"
#~ msgstr "কমপিউটার &বন্ধ করে আবার চালাও"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "দীপায়ন সরকার"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "deepayan@bengalinux.org"

#, fuzzy
#~ msgid "End Session for %1"
#~ msgstr "\"%1\"-র জন্য সেশন শেষ করো"

#, fuzzy
#~ msgid "End Session for %1 (%2)"
#~ msgstr "\"%1\"-র জন্য সেশন শেষ করো"
